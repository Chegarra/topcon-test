from .models import CarBrand
from rest_framework import serializers

class CarBrandSerializer(serializers.ModelSerializer):
  
  class Meta:
    model = CarBrand
    fields = '__all__'