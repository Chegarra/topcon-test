from .models import CarBrand
from .serializers import CarBrandSerializer
from rest_framework import viewsets, status
from rest_framework.response import Response


from django.http import JsonResponse

class CarBrandViewSet(viewsets.ViewSet):
  queryset = CarBrand.objects.all()
  serializer_class = CarBrandSerializer

  def get(self, request):
    list = CarBrand.objects.all()
    return JsonResponse(list, False)


  def post(self, request):
    serializers = self.serializer_class(data=request.data)

    if serializers.is_valid():

      return JsonResponse(
        status = status.HTTP_201_CREATED,
        success = True
      )
    else:
      return JsonResponse(
        serializers.errors,
        status = status.HTTP_400_BAD_REQUEST
      )

