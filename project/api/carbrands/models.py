from django.db import models


class CarBrand(models.Model):
    nombre = models.CharField(max_length=256)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    numero_coches = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)

    USERNAME_FIELD = 'nombre'
    REQUIRED_FIELDS = ['nombre', 'fecha_creacion', 'numero_coches', 'created_at']

    class Meta:
        ordering = ['nombre']