from .models import Carmodel
from rest_framework import serializers

class CarmodelSerializer(serializers.ModelSerializer):
  class Meta:
    model = Carmodel
    fields = '__all__'