from .models import Carmodel
from .serializers import CarmodelSerializer
from rest_framework import viewsets

class CarmodelViewSet(viewsets.ModelViewSet):
  queryset = Carmodel.objects.all()
  serializer_class = CarmodelSerializer