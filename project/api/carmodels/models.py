from django.db import models

from api.carbrands.models import CarBrand


class Carmodel(models.Model):
    nombre = models.CharField(max_length=256)
    alto = models.FloatField()
    ancho = models.FloatField()
    marca = models.ForeignKey(CarBrand, on_delete=models.CASCADE)

    USERNAME_FIELD = 'nombre'
    REQUIRED_FIELDS = ['nombre', 'alto', 'ancho', 'marca']

    class Meta:
        ordening = ['nombre']